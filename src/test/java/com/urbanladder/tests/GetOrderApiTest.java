package com.urbanladder.tests;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/*import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;*/
import org.json.JSONException;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.Method;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.urbanladder.constants.UtilityConstants;
import com.urbanladder.data.dataProvider;
import com.urbanladder.restassuredhelpers.CreateRestAssuredRequest;
import com.urbanladder.utils.CaptureResponseElements;
import com.urbanladder.utils.CreateURL;
import com.urbanladder.utils.OrderCodeGeneration;

public class GetOrderApiTest {

	@Test(dataProvider = "ApiData1")
	public void  testOrderApi(Object[] objData) throws JSONException, IOException,ParseException
	{
	/*	BasicConfigurator.configure();
		final Logger log = Logger.getLogger(PostCartApiTest.class.getName());
		Logger.getRootLogger().setLevel(Level.INFO);*/
		CreateURL url = new CreateURL();
		String urlLink = url.orderServiceURL();
	
	RequestSpecification httpRequest = RestAssured.given();
	String response = OrderCodeGeneration.responseGeneration(objData);
	httpRequest = CreateRestAssuredRequest.getRequestSpecs(response);
	String orderCode = CaptureResponseElements.orderCode(response);
	String uri = urlLink+"/"+orderCode+"?token="+UtilityConstants.token;
	//System.out.println(uri);
	Response responseGet = httpRequest.get(uri);
//	System.out.println(responseGet.statusCode());
//	System.out.println(responseGet.getBody().asString());
	Assert.assertEquals(responseGet.statusCode(),200);
	
	
	
		
		
	}
	@DataProvider(name="ApiData1")
	public static Object[] prepareData() throws IOException
	{	int i =0;
		dataProvider dataObject = new dataProvider();
		List<HashMap> data = dataObject.returnData();
		Object[] objArray = new Object[data.size()];
		
		for(int j=0;j<data.size();j++)
		{
			objArray[j] = data.get(j);
		}
		
		return objArray;
		
	}
	
	
	
	

}
