package com.urbanladder.tests;
import java.io.IOException;

/*import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;*/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONString;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.Method;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;
import com.jayway.restassured.specification.RequestSpecification;
import com.urbanladder.data.dataProvider;
import com.urbanladder.jsonUtils.JsonResponseParser;
import com.urbanladder.jsonUtils.ReadJsonRequest;
import com.urbanladder.restassuredhelpers.CreateRestAssuredRequest;
import com.urbanladder.utils.CaptureResponseElements;
import com.urbanladder.utils.CaptureResponseElements;
import com.urbanladder.utils.CreateRequestHashMap;
import com.urbanladder.utils.CreateURL;
import com.urbanladder.utils.OrderCodeGeneration;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class PostCartApiTest {

	@Test(dataProvider="ApiData")
	public static void testCartApi(Object[] obj) throws JSONException, IOException, ParseException
	{	
		/*BasicConfigurator.configure();
		final Logger log = Logger.getLogger(PostCartApiTest.class.getName());
		Logger.getRootLogger().setLevel(Level.INFO);*/

		RequestSpecification httpRequest = CreateRestAssuredRequest.postRequest(obj);
		Response response = httpRequest.post();
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode,200);
	
		//BasicConfigurator.resetConfiguration();
		
	}
	
	@DataProvider(name="ApiData")
	public static Object[] prepareData() throws IOException
	{	int i =0;
		dataProvider dataObject = new dataProvider();
		List<HashMap> data = dataObject.returnData();
		Object[] objArray = new Object[data.size()];
		
		for(int j=0;j<data.size();j++)
		{
			objArray[j] = data.get(j);
		}
		
		return objArray;
		
	}
	
	

}
