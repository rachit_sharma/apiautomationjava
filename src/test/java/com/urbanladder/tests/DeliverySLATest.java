package com.urbanladder.tests;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.urbanladder.constants.UtilityConstants;
import com.urbanladder.data.dataProvider;
import com.urbanladder.restassuredhelpers.CreateRestAssuredRequest;
import com.urbanladder.utils.*;
import com.urbanladder.utils.CreateURL;
import com.urbanladder.utils.OrderCodeGeneration;
import com.urbanladder.utils.ReadConfigFile;


public class DeliverySLATest {
	
	 static int i=0;
	 static ReadConfigFile configObject = new ReadConfigFile();
	
	@Test(dataProvider="ApiData")
	public void testSLAApi(Object[]obj) throws IOException, JSONException, ParseException 
	{	
		BasicConfigurator.configure();
		String pinCode[] = configObject.readConfigFileReturnPinCode();
		CreateURL url = new CreateURL();
		String urlLink = url.deliverySlaURL();
		String response = OrderCodeGeneration.responseGeneration(obj);
		System.out.println(response);
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest = CreateRestAssuredRequest.getRequestSpecs(response);
		String orderCode = CaptureResponseElements.orderCode(response);
		String uri = urlLink+"pincode="+pinCode[i]+"&order_number="+orderCode+"&token="+UtilityConstants.token;
		System.out.println(uri);
		Response responseGet = httpRequest.get(uri);
		System.out.println(responseGet.asString());
		Assert.assertEquals(responseGet.statusCode(),200);
		i++;
		BasicConfigurator.resetConfiguration();
		
	}
	
	@DataProvider(name="ApiData")
	public static Object[] prepareData() throws IOException
	{	int i =0;
		String pinCode[] = configObject.readConfigFileReturnPinCode();
		int length = pinCode.length;
		dataProvider dataObject = new dataProvider();
		List<HashMap> data = dataObject.returnData();
		//System.out.println(data.size());
		
		Object objArray[] = new Object[data.size()];
		
		for(int j=0;j<length;j++)
		{	
			objArray[j]= data.get(j);
			//System.out.println(objArray[j]);
		}
		
		return objArray;
		
	}

}
