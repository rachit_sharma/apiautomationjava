package com.urbanladder.restassuredhelpers;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONString;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.RequestSpecification;
import com.urbanladder.constants.UtilityConstants;
import com.urbanladder.jsonUtils.ReadJsonRequest;
import com.urbanladder.utils.CaptureResponseElements;
import com.urbanladder.utils.CreateURL;

public class CreateRestAssuredRequest {
	
	public static RequestSpecification postRequest(Object[] obj) throws JSONException, IOException
	{
		HashMap<String,String> hashMapRequest = ReadJsonRequest.readJsonForAddtoCart(obj);
		JSONObject obj1 = new JSONObject(); 
		obj1.put("variants",hashMapRequest);
		//System.out.println(obj1);
		
		CreateURL urlObject = new CreateURL();
		RestAssured.baseURI = urlObject.createAddtoCartURL();
		RestAssured.urlEncodingEnabled = false;
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.body(obj1.toJSONString());
		httpRequest.header("Content-Type","application/json");
		httpRequest.header("Accept","application/json");
		return httpRequest;
	
		
	}
	
	public static RequestSpecification  getRequestSpecs(String response) throws IOException, ParseException, JSONException
	{	
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.header("Accept","application/json");
		return httpRequest;
		
	}

}
