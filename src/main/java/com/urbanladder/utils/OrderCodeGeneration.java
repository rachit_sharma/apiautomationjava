package com.urbanladder.utils;
import java.io.IOException;
import org.json.JSONException;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;
import com.jayway.restassured.specification.RequestSpecification;
import com.urbanladder.restassuredhelpers.CreateRestAssuredRequest;




public class OrderCodeGeneration {
	
	public static String responseGeneration(Object obj) throws JSONException, IOException
	{
		PrepareOrderCodeData object = new PrepareOrderCodeData();
		Object[] objArray = object.prepareData();
	RequestSpecification httpRequest = CreateRestAssuredRequest.postRequest(objArray);
	Response response = httpRequest.post();
	ResponseBody body = response.getBody();
	String responseBody = body.asString();
	System.out.println(responseBody);
	return responseBody;

}
}