package com.urbanladder.utils;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CaptureResponseElements {
	
	public static String orderCode(String response) throws ParseException, JSONException
	{
		Object obj1 = new JSONParser().parse(response);
		 JSONObject jo = (JSONObject)obj1;
		 JSONObject data = (JSONObject) jo.get("data");
		 JSONObject order = (JSONObject) data.get("order");
		 String orderNumber = (String) order.get("number");
		System.out.println(orderNumber);
		return orderNumber;
	}

}
