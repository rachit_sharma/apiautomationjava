package com.urbanladder.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.urbanladder.data.dataProvider;

public class PrepareOrderCodeData {
	
	public static Object[] prepareData() throws IOException
	{	int i =0;
	dataProvider dataObject = new dataProvider();
	List<HashMap> data = dataObject.returnData();
	Object[] objArray = new Object[data.size()];
	
	for(int j=0;j<data.size();j++)
	{
		objArray[j] = data.get(j);
	}
	
	return objArray;
	
}

}
