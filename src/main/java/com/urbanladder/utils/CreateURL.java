package com.urbanladder.utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.urbanladder.constants.*;

public class CreateURL {
	
	ReadConfigFile configObject = new ReadConfigFile();
	
	public  String createBaseURLWulverine() throws IOException
	{
	 String 	environment  = configObject.readConfigFileReturnEnvironment();
	 
	 String url = "https://"+environment+"-wulverine.urbanladder.com/";
	 return url;	
	}
	
	
	public  String createBaseURLHercules() throws IOException
	{
	
		String 	environment  = configObject.readConfigFileReturnEnvironment();
	 String url = "https://"+environment+"-hercules.urbanladder.com";
	 return url;	
	}
	
	
	public String createConsumerURL() throws IOException
	{
	String	url = this.createBaseURLWulverine();
		return url = url+"v1/consumers/";
		
	}
	
	public String createOrderURL() throws IOException
	{
		String	url = this.createBaseURLWulverine();
		return url = url+"v1/orders/";
		
	}
	
	public String createCRMURL() throws IOException
	{
		String	url = this.createBaseURLWulverine();
		return url = url+"v2/leads/";
		
	}
	
	
	public String createAddtoCartURL() throws IOException
	{
		String url = this.createBaseURLHercules();
		//System.out.println(url+"cart");
		return url = url+"/cart";
		
	}
	
	public String orderServiceURL() throws IOException
	{
		String url = this.createBaseURLHercules();
		//System.out.println(url+"cart");
		return url = url+"/api/orders";
	}
	
	public String deliverySlaURL() throws IOException
	{
		String url = this.createBaseURLHercules();
		//System.out.println(url);
		return url = url+"/api/v1/serviceability?sla_type=individual&";
		
	}
	
	
	

}
