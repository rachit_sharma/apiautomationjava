package com.urbanladder.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import com.urbanladder.data.dataProvider;

public class ReadConfigFile {

	public String readConfigFileReturnEnvironment() throws IOException
	{
		File file = new File("/Users/rachitsharma/Desktop/newApiTesting1/urbanladder/config.properties");
		FileInputStream fileInput = null;
		 fileInput = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fileInput);
		
		String environment = prop.getProperty("environment");
		return environment;
		
	}
	
	public String[] readConfigFileReturnPinCode() throws IOException
	{	
		dataProvider dataObject = new dataProvider();
		List<HashMap> data = dataObject.returnData();
		
		File file = new File("/Users/rachitsharma/Desktop/newApiTesting1/urbanladder/config.properties");
		FileInputStream fileInput = null;
		 fileInput = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fileInput);
		String[] pinCode = prop.getProperty("Pincodes[]").split(",");
		return pinCode;
		
	}
	
}
