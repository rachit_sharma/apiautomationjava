package com.urbanladder.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

//import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class dataProvider {
	
	//Utility to Read data from Excel sheet and convert it to list of hashmap.
	
	
	private static final String FILE_NAME = "/Users/rachitsharma/Downloads/TestData.xlsx";
	ArrayList<String> column_Names = new ArrayList<String>();
	int count=0;
	@SuppressWarnings("deprecation")
	public  List<HashMap> returnData() throws IOException
	{   
		ArrayList<HashMap> dataMap1= new ArrayList<HashMap>();
		int i=0;
		FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
        Workbook workbook = new XSSFWorkbook(excelFile);
        Sheet datatypeSheet = workbook.getSheetAt(0);
       // System.out.println(datatypeSheet.getSheetName());
        //Initializing a row iterator to go through each row       
        Iterator<Row> iterator = datatypeSheet.iterator();
        while(iterator.hasNext())
        {
        	Row currentRow = iterator.next();
        	
        int j =0;
        //Initializing a cell iterator to traverse through each column of a row.
        
        Iterator<Cell> cellIterator = currentRow.iterator();
        
        if(currentRow.getRowNum()!=0) //Checking if row no. is not equal to 0
        	
        {
        	HashMap dataMap= new HashMap ();
        	
        while (cellIterator.hasNext()) 
        { 
        	
        	Cell currentCell = cellIterator.next();
        
  //Check if the Cell data contains String or Numeric value and use respective methods to store the info
           
        if (currentCell.getCellTypeEnum() == CellType.STRING) 
        { 
               dataMap.put(column_Names.get(j),currentCell.getStringCellValue());
               j++;
               
               if(j== (datatypeSheet.getRow(0).getPhysicalNumberOfCells()))
               {	           
               	 dataMap1.add((currentRow.getRowNum()-1),dataMap);
               }
        } 
        
        else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) 
        {
            	 int currentCellValue = (int) ((currentCell.getNumericCellValue()));
            	
            	dataMap.put(column_Names.get(j),currentCellValue);
            	j++;
            	
            	if(j==(datatypeSheet.getRow(0).getPhysicalNumberOfCells()))
                { 	
                	    dataMap1.add(currentRow.getRowNum()-1,dataMap);;
 
                }

       }
        
        }
        
        }
        
        //Condition to check if row number is 0, because for 0th row there is no info to be captured.
        else if(currentRow.getRowNum()==0)
        {
        	while (cellIterator.hasNext()) {
        		Cell currentCell = cellIterator.next();
        		column_Names.add(i,currentCell.getStringCellValue());
        		i++;
        	}
        	 
        }
        
        }
        
//System.out.println(dataMap1);
		return dataMap1;
        	
	}
	

	
		
       	
}
	

